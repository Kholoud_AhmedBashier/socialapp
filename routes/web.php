<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('emotions', [
    'uses' => 'moodsController@emotionsList'
]);

Route::post('addMood', [
    'uses' => 'moodsController@addMood'
]);

Route::post('addGift', [
    'uses' => 'giftsController@addGift'
]);

Route::get('giftsList/{id}', [
    'uses' => 'giftsController@giftsList'
]);

Route::get('showGift/{id}', [
    'uses' => 'giftsController@showGift'
]);

Route::post('showLogs',['uses' => 'LogsController@viewLogs', 'as' => 'viewLogs']);

