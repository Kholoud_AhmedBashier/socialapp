<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', ['uses' => 'AuthController@login', 'as' => 'login']); //User Login
Route::post('/register', ['uses' => 'AuthController@register', 'as' => 'signup']); //User Registration
Route::post('/set-token', ['uses' => 'AuthController@setToken', 'as' => 'set-token']);//Set Device Token For User


Route::post('/addComment', ['uses' => 'CommentController@addComment', 'as' => 'moodComment']); //User Login
Route::post('openProfile',['uses' => 'ProfileController@openProfile', 'as' => 'openProfile']);
Route::post('showLogs',['uses' => 'LogsController@viewLogs', 'as' => 'viewLogs']);
Route::post('showFriends',['uses' => 'FriendsController@friendsList', 'as' => 'showFriends']);

Route::get('dayFriend',['uses' => 'FriendsController@dayFriend', 'as' => 'dayFriend']);

Route::get('emotions', [
    'uses' => 'moodsController@emotionsList'
]);

Route::post('addMood', [
    'uses' => 'moodsController@addMood'
]);

Route::post('addGift', [
    'uses' => 'giftsController@addGift'
]);

Route::get('giftsList/{id}', [
    'uses' => 'giftsController@giftsList'
]);

Route::get('showGift/{id}', [
    'uses' => 'giftsController@showGift'
]);
