<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->default('assets/images/users/default.png');
            $table->string('bg')->default('assets/images/users/default.png');
            $table->string('name');
            $table->string('user_name');
            $table->string('email')->unique();
            $table->string('mobile')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('password');

            $table->string('address')->nullable();
            $table->double('lat')->default(0);
            $table->double('long')->default(0);

            $table->string('facebook')->nullable()->default(null);
            $table->string('twitter')->nullable()->default(null);
            $table->string('instagram')->nullable()->default(null);
            $table->string('google')->nullable()->default(null);
            $table->string('youtube')->nullable()->default(null);

            $table->integer('no_views')->default(0);
            $table->integer('no_friends')->default(0);

            $table->boolean('gender')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_notification')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
