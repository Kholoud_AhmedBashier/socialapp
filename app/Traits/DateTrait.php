<?php

namespace App\Traits;

use Carbon\Carbon;
use Exception;

trait DateTrait
{
    public $english_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'AM', 'PM'.'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    public $arabic_numbers = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩', 'صباحاً' , 'مساءً','يناير','فبراير','مارس','أبريل','مايو','يونيو','يوليو','اغسطس','سبتمبر','اكتوبر','نوفمبر','ديسيمبر'];

    public function replaceByLocale($locale)
    {
        switch ($locale) {
            case 'ar' :
                return $this->arabic_numbers;
                break;
            case 'en' :
                return $this->english_numbers;
                break;
            default :
                return $this->english_numbers;
        }
    }

    public function readableDate($date_from)
    {
        $result = Carbon::createFromTimeStamp(strtotime($date_from))->diffForHumans();
        return $result;
    }

    public function getNiceDate($date, $format = 'D j M Y')
    {
        return Carbon::parse($date)->format($format);
    }

    public function getNiceTime($time, $format = 'g:i A')
    {
        return Carbon::parse($time)->format($format);
    }

    public function validateDate($date, $format = "Y-M-D")
    {
        try {
            $date = Carbon::parse($date)->format($format);
        } catch (Exception $ex) {
            return false;
        }

        return true;
    }

    public function getDifferenceInDays($date)
    {
        $now = Carbon::now();
        $length = $date->diffInDays($now);
        return $length;
    }
}