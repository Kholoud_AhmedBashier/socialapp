<?php

namespace App\Traits;
use App\Mail\SuperMailer;
use Mail;
use Nexmo;

trait NotificationTrait
{
    public function __construct(){
        define( 'API_ACCESS_KEY', 'AAAAS0Dv_oQ:APA91bE8zDhk2DkrbUhIr_2tn24lX4lep2ygX_Fo9tswjfvGc89pbfANbzOUrgMKoMpSiL63Qx8Ar33O9rVixaQgKvRaQg61aZxn6jutG2OYNxKVoYbpvG9-Vrh6SyNUXLrGwLZ8gPds' );
    }

    public  function  sendMail($data){
        Mail::to($data['to'])->send(new SuperMailer($data));
    }

    public function sendSMS($code, $from, $to, $message){
        Nexmo::message()->send([
            'to' => $code . $to,
            'from' => $from,
            'text' => $message,
            'type'     => 'unicode',
        ]);
    }


    public function push_notification($tite, $body, $action, $notification, $tokens){
        #prep the bundle
        $msg = array
        (
            'body' 	            => $body,
            'title'	            => $tite,
            'action' 	        => $action,
            'notification_id'	=> $notification->id,
            'subtitle' => '',
            'tickerText' => '',
            'msgcnt' => 1,
            'vibrate' => 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon',
        );


        $fields = array
        (
            'registration_ids'		=> $tokens,
            // to => $token,
            'notification'	=> $msg,
            'data'	=> $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }

    public function notificationMessage($lang, $key){
        $arabic = [

        ];

        $english = [

        ];

        if($lang == 'ar'){
            return $arabic[$key];
        }

        return $english[$key];
    }
}