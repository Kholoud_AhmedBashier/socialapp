<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //

    protected $table = 'logs';

    protected $fillable = [
        'user_id','object_type','object_id'
    ];

    function users() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    function moods() {
        return $this->hasOne('App\Mood', 'id', 'object_id');
    }

    function gifts() {
        return $this->hasOne('App\Gift', 'id', 'object_id');
    }

}
