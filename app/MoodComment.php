<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoodComment extends Model
{
    //

    protected $fillable = [
         'mood_id','user_id','comment'
    ];

}
