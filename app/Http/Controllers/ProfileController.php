<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Super;
use App\User;

class ProfileController extends Controller
{
    //
    //use SuperTrait;

    public function openProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Id is mandatory', $resposnse);
        }
        $user = User::where('id',$request->id)->first();

        if (!$user) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Success !", $user);
        }


    }
}
