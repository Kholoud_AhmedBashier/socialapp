<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Super;
use App\Friend;
class FriendsController extends Controller
{
    //

    public function friendsList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'User id is required', $resposnse);
        }
        $friends = Friend::where('sender_id',$request->user_id)->get();

        if (!$friends) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Success !", $friends);
        }
    }

    public function dayFriend()
    {

        $resposnse = new \stdClass();
        $friend = Friend::inRandomOrder()->first();
        if (!$friend) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Success !", $friend);
        }
    }
}
