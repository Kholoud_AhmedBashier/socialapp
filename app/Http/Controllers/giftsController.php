<?php

namespace App\Http\Controllers;

use App\Mood;
use App\Gift;
use App\Emotion;
use Illuminate\Http\Request;
use Validator;
use Super;


class giftsController extends Controller {

    public function addGift (Request $request){

        $text = $request->input('text');
        $gift_body = $request->input('gift_body');
        $gift_type = $request->input('gift_type');
        $gift_status = $request->input('gift_status');
        $mood_id = $request->input('mood_id');
        $user_id = $request->input('user_id');
        $receiver_id = $request->input('receiver_id');

        $rules = array(
            'mood_id' => 'required',
            'user_id' => 'required',
            'receiver_id' => 'required',
        );

        $validator = Validator::make( $request->all(), $rules );
        $validatorMessages = $validator->errors()->messages();
        $validatorStatus = $validator->fails() ? 0 : 1;
        $insertStatus = 0;

        if($validatorStatus){
            $m = new Gift;
            $m->text = $text;
            $m->gift_body = $gift_body;
            $m->gift_type = $gift_type;
            $m->gift_status = $gift_status;
            $m->mood_id = $mood_id;
            $m->user_id = $user_id;
            $m->receiver_id = $receiver_id;
            if($m->save()){
                $insertStatus = 1;
            }
        }

        $processStatus = ($insertStatus && $validatorStatus);

        $status =  $processStatus ? 1 : 0;
        $error_code = ($processStatus) ? 200 : 400 ;
        $validation = $validatorMessages;
        $message = ($processStatus) ? 'success' : 'fail process' ;
        $response = $processStatus ? 1 : 0;

        return Super::jsonResponse($status, $error_code, $validation, $message, $response);

    }

    public function giftsList($userId){

        $gift = Gift::with('comments')
                        ->where('receiver_id', $userId)
                        ->get();

        $giftStatus = (empty($data));
        $status = $giftStatus;
        $error_code = ($giftStatus) ? 200 : 400 ;
        $validation = '';
        $message = ($giftStatus) ? 'success' : 'fail process' ;
        $response = $gift;

        return Super::jsonResponse($status, $error_code, $validation, $message, $response);

    }

    public function showGift($id){

        $gift = Gift::find($id);
        $giftStatus = (empty($data));

        $status = $giftStatus;
        $error_code = ($giftStatus) ? 200 : 400 ;
        $validation = '';
        $message = ($giftStatus) ? 'success' : 'fail process' ;
        $response = $gift;

        return Super::jsonResponse($status, $error_code, $validation, $message, $response);

    }

}