<?php

namespace App\Http\Controllers;

use App\Mood;
use App\Emotion;
use Illuminate\Http\Request;
use Validator;
use Super;


class moodsController extends Controller {

    public function addMood(Request $request){

        $user_id = $request->input('user_id');
        $emotion_id = $request->input('emotion_id');
        $desc = $request->input('desc');

        $rules = array(
            'user_id'  => 'required',
            'emotion_id'  => 'required',
            'desc' => 'required',
        );

        $validator = Validator::make( $request->all(), $rules );
        $validatorMessages = $validator->errors()->messages();
        $validatorStatus = $validator->fails() ? 0 : 1;
        $insertStatus = 0;

        if($validatorStatus){

            $m = new Mood;
            $m->user_id =$user_id ;
            $m->emotion_id =$emotion_id ;
            $m->desc =$desc ;
            if($m->save()){
                $insertStatus = 1;
            }

        }

        $processStatus = ($insertStatus && $validatorStatus);
        $status =  $processStatus ? 1 : 0;
        $error_code = ($processStatus) ? 200 : 400 ;
        $validation = $validatorMessages;
        $message = ($processStatus) ? 'success' : 'fail process' ;
        $response = $processStatus ? 1 : 0;

        return Super::jsonResponse($status, $error_code, $validation, $message, $response);

    }


    public function emotionsList(){

        $emotions = Emotion::get();
        $emotionsStatus = (empty($data));

        $status = $emotionsStatus;
        $error_code = ($emotionsStatus) ? 200 : 400 ;
        $validation = '';
        $message = ($emotionsStatus) ? 'success' : 'fail process' ;
        $response = $emotions;

        return Super::jsonResponse($status, $error_code, $validation, $message, $response);

    }

}