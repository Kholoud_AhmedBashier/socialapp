<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Super;
use App\Log;

class LogsController extends Controller
{
    //

    public function viewLogs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'User id is required', $resposnse);
        }

        $logs = Log::with(['users', 'moods', 'gifts'])
                ->where('user_id', $request->user_id)
                ->get();

        if (!$logs) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Success !", $logs);
        }

    }

}
