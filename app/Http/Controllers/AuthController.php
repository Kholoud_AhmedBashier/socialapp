<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\App;
use Validator;
use Super;
use Mail;
use App\Token;

class AuthController extends Controller
{
    /*
     *set Languages
     * input parameter : lang -
     * cases: ar for (arabic) en for (english) default (en)
     *
     */
    public function __construct(Request $request)
    {
        if (!isset($request->lang)) {
            $request->lang = "en";
        }
        if ($request->lang != "ar" && $request->lang != "en") {
            return Super::jsonResponse(false, 503, [], 'Bad Request (Un Expected Values)', []);
        }
        /*        App::setLocale($request->lang);*/
    }

    /*
     * User Login
     * input parameter : email - password
     * cases :  Authenticated -  Authenticated but not active
     * output : user object or []
     *
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', $resposnse);
        }
        if ((Auth::attempt(['email' => request('email'), 'password' => request('password')])) ||
            (Auth::attempt(['mobile' => request('email'), 'password' => request('password')]))
        ) {

            if (Auth::user()->is_active == 1) {
                return Super::jsonResponse(true, 0, [], 'Login', Auth::user());
            } else {
                return Super::jsonResponse(true, 0, [], "Sorry your account dis activated", Auth::user());
            }
        } else {
            return Super::jsonResponse(false, 3, [], "wrong in user name or password", $resposnse);
        }
    }

    /*
     * user Registration
     * input parameter : name - email - password
     * cases :  already Registered - registration successfully
     * output : user object or []
     *
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'user_name' => 'required'

        ]);
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', []);
        }
        $resposnse = new \stdClass();
        $if_exist = User::where('email', $request->email)->first();
        if ($if_exist) {
            return Super::jsonResponse(false, 4, [], trans('This account already exists'), $resposnse);
        }
        $request['is_notification'] = 1;
        $request['is_active'] = 1;
        $user = User::create($request->all());
        //return $request->all();
        if (!$user) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Welcome you !", $user);
        }
    }

    /*
     * Active Account (activation code that sent as sms to user)
     * input parameter : mobile - verification_code
     * cases : Wrong Code - Activation Code
     * output : user object or []
     */

    public function activeAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'verification_code' => 'required',
        ]);
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', []);
        }
        $resposnse = new \stdClass();

        $user = User::where('email', $request['email'])->orWhere('mobile', $request['email'])->first();
        if ($user) {
            if ($user->verification_code == $request['verification_code']) {
                $user->is_confirm = 1;
                $user->save();
                return Super::jsonResponse(true, 0, [], trans('lang.activate_success'), $user);
            } else {
                return Super::jsonResponse(false, 500, [], trans('lang.activate_error'), $resposnse);
            }
        }
        return Super::jsonResponse(false, 204, [], trans('lang.user_notfound'), $resposnse);
    }

    /*
     * Facebook Login - Register
     *  input parameter : facebook_id
     * cases : Login - Register
     *
     */
    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider' => 'required',
            'provider_id' => 'required',
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', []);
        }
        $resposnse = new \stdClass();

        $user = User::where('provider_id', $request->provider_id)->first();

        if ($user) {//Login With Social
            Auth::loginUsingId($user->id);
            return Super::jsonResponse(true, 0, [], trans('lang.login_success'), $user);

        } else //Sighnup with Social
        {
            $if_exist = User::where('mobile', $request->mobile)->first();
            if ($if_exist) {
                return Super::jsonResponse(false, 4, [], trans('lang.signup_error_mobile'), $resposnse);
            }
            $if_exist = User::where('email', $request->email)->first();
            if ($if_exist) {
                return Super::jsonResponse(false, 4, [], trans('lang.signup_error_email'), $resposnse);
            }
            $request['password'] = bcrypt("password");
            if (!isset($request->email)) {
                $request['email'] = time() . "@zteck.com";
            }
            $request['is_active'] = 1;
            $request['is_notification'] = 1;
            $user = User::create($request->all());
            Auth::loginUsingId($user->id);
            return Super::jsonResponse(true, 0, [], trans('lang.login_success'), $user);
        }
    }

    /*
     * Set Device Token
     * input parameter:user_id -device_token
     * cases : Token already exist - new device token
     * output:[]
     */

    public function setToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', []);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return Super::jsonResponse(false, 204, $validator->errors(), 'User may be deleted', []);
        }
        $user_tokens = Token::where('user_id', $request->user_id)->where('token', $request->token)->get()->first();
        if (!$user_tokens) {
            Token::create($request->all());
        }
        return Super::jsonResponse(true, 0, [], "done", []);
    }

    public function Logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required,id',
            'serial' => 'required',
        ]);

        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', []);
        }
        $user_tokens = Token::where('user_id', $request->user_id)->where('serial', $request->serial)->get();
        foreach ($user_tokens as $token) {
            $token->delete();
        }
        return Super::jsonResponse(true, 0, [], trans('lang.logout_success'), []);
    }

}