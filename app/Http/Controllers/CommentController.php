<?php

namespace App\Http\Controllers;

use App\MoodCommentLike;
use Illuminate\Http\Request;
use Validator;
use Super;
use App\MoodComment;
class CommentController extends Controller
{
    //

    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'mood_id' => 'required',
        ]);
        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', $resposnse);
        }
        $mood_comment = MoodComment::create($request->all());

        if (!$mood_comment) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Comment Added !", $mood_comment);
        }


    }
    public function addGiftComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'mood_id' => 'required',
        ]);
        $resposnse = new \stdClass();
        if ($validator->fails()) {
            return Super::jsonResponse(false, 400, $validator->errors(), 'Error in validation', $resposnse);
        }
        $mood_comment = MoodComment::create($request->all());

        if (!$mood_comment) {
            return Super::jsonResponse(false, 500, [], "Sorry unexpected error", $resposnse);
        } else {
            return Super::jsonResponse(true, 0, [], "Comment Added !", $mood_comment);
        }


    }

}
