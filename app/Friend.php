<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\User;
class Friend extends Model
{
    //
    protected $table = 'friends';

    protected $fillable = [
        'sender_id','receiver_id'
    ];

//    public function Users()
//    {
//        return $this->hasMany('App\User');
//    }
}
