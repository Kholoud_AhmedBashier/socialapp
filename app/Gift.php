<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{

    protected $table = 'gifts';

    // gift comments
    function comments() {
        return $this->hasMany('App\GiftComment', 'gift_id', 'id');
    }

}
