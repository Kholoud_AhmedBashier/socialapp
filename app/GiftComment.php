<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftComment extends Model
{

    protected $table = 'gift_comments';

}
